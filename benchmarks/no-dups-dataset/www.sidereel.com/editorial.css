/* Screen Sizes */


/* Responsive YouTube Embed */

.hs-responsive-embed-youtube {
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 25px;
  height: 0;
}

.hs-responsive-embed-youtube iframe {
  position: absolute;
  top: 0;
  left: 0;
  width: 100% !important;
  height: 100% !important;
}


/* Amazon Prime Ads */

a[title="SideReel Works Better With Amazon Prime"] img {
  margin-bottom: 10px;
}


/* Networks Sidebar */

.e-networks-sidebar-module .networks .row.collapse {
  margin-bottom: 10px;
}

.e-networks-sidebar-module .networks-broadcast .secondary-links a {
  height: 100%;
}

.e-networks-sidebar-module .secondary-links a {
  width: 100%;
  text-align: center;
  padding: 0;
}

.e-networks-sidebar-module .secondary-links.columns {
  padding: 0 0.2rem;
}


/* Genres & Networks Pages */

.e-genres-networks {
  position: relative;
}

.e-genres-networks .columns {
  background-color: #55abca;
  padding: 0;
}

.e-genres-networks .columns:hover {
  background-color: #0080ae;
}

.e-genres-networks img {
  width: 100%;
  display: block;
}

.e-genres-networks h3 {
  color: white;
  position: absolute;
}

@media only screen and (max-width: 47.937em) {
  .e-genres-networks .columns {
    border: 1px solid white;
  }
  .e-genres-networks .columns h3 {
    font-size: 1.2rem;
    padding: 0;
    top: 3px;
    left: 4px;
    line-height: 1.1rem;
  }
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-genres-networks .columns {
    border: 2px solid white;
  }
  .e-genres-networks .columns h3 {
    font-size: 1.5rem;
    padding: 3px 0 0 3px;
    top: 0px;
    left: 7px;
    line-height: 1.5rem;
  }
}

@media only screen and (min-width: 68em) {
  .e-genres-networks .columns {
    border: 2px solid white;
  }
  .e-genres-networks .columns h3 {
    font-size: 2rem;
    padding: 3px 0 0 3px;
    line-height: 2rem;
    top: 2%;
    left: 6%;
  }
}

.e-genres-networks-no-pic .columns {
  background-color: #55abca;
  color: white;
  text-align: center;
  border: solid white;
  padding: 0;
}

.e-genres-networks-no-pic .columns:hover {
  background-color: #0080ae;
}

.e-genres-networks-no-pic .columns .no-pic-text {
  position: absolute;
  width: 100%;
}

@media only screen and (max-width: 47.937em) {
  .e-genres-networks-no-pic .columns {
    font-size: 1.1rem;
    line-height: 1.1rem;
    height: 38px;
  }
  .e-genres-networks-no-pic .columns .no-pic-text {
    top: 26%;
  }
  .e-genres-networks-no-pic .columns .no-pic-text.two-line {
    top: 4%;
  }
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-genres-networks-no-pic .columns {
    border-width: 2px;
    font-size: 1.6rem;
    line-height: 1.6rem;
    height: 85px;
  }
  .e-genres-networks-no-pic .columns .no-pic-text {
    top: 42%;
  }
  .e-genres-networks-no-pic .columns .no-pic-text.two-line {
    top: 26%;
  }
}

@media only screen and (min-width: 68em) {
  .e-genres-networks-no-pic .columns {
    border-width: 2px;
    font-size: 2rem;
    height: 176px;
    padding: 0;
  }
  .e-genres-networks-no-pic .columns .no-pic-text {
    top: 47%;
  }
  .e-genres-networks-no-pic .columns .no-pic-text.two-line {
    top: 41%;
    line-height: 1.8rem;
  }
}


/* 5 Reasons Page */

.e-5-reasons {
  padding: 5px;
}


/* News & Press */

.e-press-and-news ul {
  margin-left: 0;
}

.e-press-and-news ul li {
  list-style-type: none;
  margin-bottom: 0.5rem;
}

.e-press-and-news img {
  border: 1px solid #ccc;
  padding: 2px;
  margin-bottom: 0.5rem;
}

.e-press-and-news .row {
  margin-bottom: 1rem;
  border-bottom: 1px solid #dddfdd;
}

.e-press-and-news .row .columns {
  padding-left: 0;
}

.e-press-and-news .news-source {
  font-size: 12px;
}

.e-press-and-news .press-date {
  font-size: 12px;
  font-weight: bold;
}


/* Team Page */

.e-team .name {
  text-align: center;
  font-weight: bold;
  margin-top: 0.5rem;
}

.e-team .title {
  text-align: center;
  max-width: 160px;
}

@media only screen and (max-width: 47.937em) {
  .e-team .title {
    max-width: 140px;
  }
}

.e-team ul li {
  margin-left: 0.5rem;
  height: 250px;
}

@media only screen and (max-width: 47.937em) {
  .e-team ul li {
    height: 230px;
  }
  .e-team ul li img {
    width: 140px;
  }
}


/* 5 Reasons Sidebar */

.e-5-reasons-sidebar {
  margin-bottom: 10px !important;
}

.e-5-reasons-sidebar img {
  width: 100%;
  padding: 0;
}

.e-5-reasons-sidebar h2 {
  font-size: 20px;
  color: #0080ae;
  padding: 0;
  margin: 0.5rem 0;
}

.e-5-reasons-sidebar p {
  padding-bottom: 5px;
}


/* Movie page */

.e-movies h1 {
  font-size: 2.5rem;
  line-height: 3rem;
  margin-bottom: 0.5rem;
}

.e-movies .row {
  padding: 1rem 0;
}

.e-movies .projector-graphic {
  padding: 1rem 0;
}

.e-movies .projector {
  height: 160px;
  padding-top: 40px;
  padding-right: 10px;
}

.e-movies .all-movie-button {
  margin: 1rem 0;
  text-align: center;
}

.e-movies .all-movie-logo img {
  height: 160px;
}


/* Connectivity Test */

.e-connectivity-test .image-collection {
  margin: 1rem;
}

.e-connectivity-test .image-collection img {
  padding: 1rem;
  border: 2px solid #0080ae;
  background-color: #ddd;
  margin: 0 1rem 1rem 0;
}


/* Jobs Page */

.e-jobs ul {
  margin-left: 1.5rem;
}

.e-jobs .logos img {
  margin-right: 20px;
  margin-top: 10px;
}


/* Mobile Page */

.e-mobile p {
  font-size: 1.2rem;
}

.e-mobile ul li {
  margin: 1rem;
  font-size: 1.2rem;
}


/* Content Owners */

.e-content-owners .contact-us {
  border: 1px solid #ddd;
  margin-bottom: 1rem;
  padding: 1rem;
}

.e-content-owners .contact-us .method {
  text-transform: uppercase;
  font-weight: bold;
  width: 5.5rem;
  display: inline-block;
}

.e-content-owners .contact-us img {
  vertical-align: text-bottom;
}

.e-content-owners .contact-us .value {
  display: inline-block;
}

.e-content-owners .partner-with-us {
  background-color: #eef7fa;
}

.e-content-owners .section-container {
  background-color: #ededef;
  clear: both;
}

.e-content-owners .section-container > .columns {
  padding: 1rem;
}

.e-content-owners h3 {
  font-size: 14px;
  margin-bottom: 0.5rem;
  margin-top: 1rem;
}

.e-content-owners h3 img {
  margin-right: 0.5rem;
}


/* Fall-Summer-Midseason Preview pages */

.e-preview .module-header {
  margin-bottom: 0px;
}

.e-preview p.intro {
  border-bottom: 1px solid #cacbcd;
  margin-bottom: 10px;
  padding-bottom: 10px;
  font-size: 1.2rem;
}

.e-preview.row {
  border-bottom: 1px solid #cacbcd;
  padding-bottom: 10px;
  padding-top: 10px;
}

.e-preview.row:last-child {
  border-bottom: none;
}

.e-preview .columns.poster {
  padding-right: 10px;
  padding-left: 0;
}

@media only screen and (max-width: 47.937em) {
  .e-preview .columns.poster {
    padding-bottom: 10px;
  }
}

.e-preview .columns.info {
  line-height: 1.4rem;
  padding: 0 5px 5px 0;
}

.e-preview .columns.info a {
  font-weight: normal;
}

.e-preview .comment-text {
  background: #f6f8f6;
  border: #dddfdd solid 1px;
  padding: 4px 7px;
  position: relative;
  width: 80%;
  height: 70px;
  margin-bottom: 0px;
  float: left;
}

.e-preview .comment-text:before {
  content: '';
  position: absolute;
  border-style: solid;
  border-width: 8px 0 8px 10px;
  border-color: transparent #dddfdd;
  display: block;
  width: 0;
  z-index: 0;
  right: -11px;
  top: 13px;
}

.e-preview .comment-text:after {
  content: '';
  position: absolute;
  border-style: solid;
  border-width: 8px 0 8px 10px;
  border-color: transparent #f6f8f6;
  display: block;
  width: 0;
  z-index: 1;
  right: -10px;
  top: 13px;
}

.e-preview .comment-text .Pick {
  color: #6fa049;
}

.e-preview .comment-text .Pan {
  color: #b6312a;
}

@media only screen and (max-width: 47.937em) {
  .e-preview .comment-text {
    height: auto;
  }
}

.e-preview .editor-icon {
  float: right;
  width: 15%;
}

.e-preview .editor-icon img {
  border: solid 2px #6fa049;
  width: 100%;
}

.e-preview .editor-icon.contributor img {
  border: solid 2px #78698f;
}

.e-preview .description {
  margin-bottom: 0;
  margin-top: 5px;
}


/* This Week in TV */

.e-twitv .star i {
  font-size: 1rem;
}

.e-twitv h3 {
  font-size: 1.1rem;
  padding-bottom: 2px;
}

.e-twitv .module-header {
  margin-bottom: 0;
}

@media only screen and (max-width: 47.937em) {
  .e-twitv.choose-day .title {
    padding: 0 0 6px 0;
  }
}

@media only screen and (min-width: 47.938em) {
  .e-twitv.choose-day .title {
    padding: 7px 0;
    font-size: 1.5rem;
  }
}

.e-twitv.choose-day h2 {
  font-size: 1.1rem;
  width: 100%;
}

.e-twitv.choose-day a {
  color: white;
  background-color: #55abca;
  text-align: center;
  border: white 2px solid;
  padding: 10px 0;
}

.e-twitv.choose-day a:hover {
  background-color: #0080ae;
}

.e-twitv.choose-day .module-header {
  border-bottom: none;
}


/* FAQ */

.e-faq .faq-label {
  font-weight: bold;
  font-family: Montserrat, Helvetica, Arial, sans-serif;
}

.e-faq h3 {
  margin-top: 0.5rem;
}

.e-faq dt {
  margin-top: 2rem;
}

.e-faq li {
  margin-bottom: .5rem;
  margin-left: .5rem;
}

.e-faq li a {
  font-weight: normal;
}


/* Recommendations */

.e-recommendations ul li {
  width: 22.9%;
  margin-left: 0rem;
  margin-right: 1rem;
}

.e-recommendations ul li:last-child {
  margin-right: 0;
}

.e-recommendations ul li img {
  margin-bottom: 3px;
}

@media only screen and (max-width: 47.937em) {
  .e-recommendations ul li {
    width: 21.5%;
  }
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-recommendations ul li {
    width: 22.4%;
  }
}

.e-recommendations .section-text {
  padding-right: 1rem !important;
}


/* Featured Shows */

.e-featured-shows h2 {
  width: 100%;
}

.e-featured-shows .featured-show.show-for-medium-only:not(:first-child) {
  border-top: 1px solid #DDDFDD;
  padding-top: 10px;
}

@media only screen and (max-width: 47.937em) {
  .e-featured-shows .featured-show:not(: first-child) {
    border-top: 1px solid #DDDFDD;
    padding-top: 10px;
  }
}

.e-featured-shows .featured-show img {
  margin-bottom: 10px;
}

.e-featured-shows .featured-show h3 {
  font-size: 15px;
  line-height: 1.25rem;
  margin-bottom: .25rem;
}

.e-featured-shows .featured-show p {
  text-align: justify;
}


/* Home Page CMS */

.e-intro-widgets a.button {
  margin: 0.5rem 0;
}

.e-intro-widgets img {
  margin: 0 0.5rem 0.5rem 0.5rem;
}

.e-intro-widgets p {
  font-size: 14px;
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-intro-widgets p {
    font-size: 15px;
  }
}

.e-intro-widgets .row {
  margin-bottom: 1rem;
}

@media only screen and (min-width: 68em) {
  .e-intro-widgets .left-module {
    padding-right: 10px !important;
  }
  .e-intro-widgets .partial-bordered-module {
    min-height: 365px;
  }
}

.e-sister-sites {
  margin-top: 10px;
}

@media only screen and (min-width: 47.938em) {
  .e-sister-sites p {
    margin-top: 0.5rem;
    margin-bottom: 1.5rem;
  }
}

@media only screen and (max-width: 47.937em) {
  .e-sister-sites img {
    padding: 0 10px 10px 0;
  }
}


/* Trending Shows (by genre) */

.e-trending #tracked_shows {
  line-height: 0.8;
  text-align: center;
  margin: 50px 0 100px;
}

@media only screen and (max-width: 47.937em) {
  .e-trending p.description {
    margin-top: 15px;
  }
  .e-trending .e-trending .row.colorstripe {
    margin: 0;
  }
  .e-trending .e-trending #tag_cloud {
    padding: 0;
  }
}

.e-trending #tag_cloud {
  clear: both;
  padding: 50px 0;
}

.e-trending .word {
  cursor: pointer;
  font-family: "Montserrat", "Trebuchet MS", Helvetica, Arial, sans-serif;
  text-decoration: none;
  display: inline-block;
  position: relative;
  margin: 0.25em 0.5em;
  padding: 1em;
  border-radius: 5px;
  transition: color 0.2s, background 0.2s;
  letter-spacing: -1px;
}

.e-trending .word.all {
  color: #383b43;
}

.e-trending .word.comedy {
  color: #f19144;
}

.e-trending .word.crime {
  color: #ea6c67;
}

.e-trending .word.reality {
  color: #70607e;
}

.e-trending .word.action {
  color: #70607e;
}

.e-trending .word.drama {
  color: #55abbd;
}

.e-trending .word.sci-fi {
  color: #869928;
}

.e-trending .word.filtered {
  color: #f0f0f0;
  cursor: default;
}

.e-trending .bubble {
  display: none;
}

.e-trending .bubble h2 {
  line-height: 1.5rem;
  margin-bottom: 0.5rem;
}

.e-trending .bubble-wrap {
  position: relative;
  overflow: hidden;
  float: left;
}

.e-trending .bubble-wrap img {
  float: left;
  width: 110px;
}

.e-trending .bubble-content [class*="details-"] {
  line-height: 1.5rem;
}

.e-trending .bubble-content .details,
.e-trending .bubble-content h5 {
  padding-left: 120px;
}

.e-trending .bubble-content .details-tracks {
  font-weight: bold;
}

.e-trending .show-description {
  padding-top: 10px;
  text-align: justify;
  clear: both;
}

.e-trending .colorstripe:after {
  content: '';
  display: block;
  clear: both;
}

.e-trending .colorstripe li {
  background: #282830;
}

.e-trending .colorstripe li a {
  font-family: "Montserrat", "Trebuchet MS", Helvetica, Arial, sans-serif;
  text-decoration: none;
  text-align: center;
  cursor: pointer;
  padding: 1em;
  color: #fff;
  transition: all 0.2s;
  letter-spacing: -1px;
  display: block;
}

.e-trending .colorstripe li a.visible {
  display: block;
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-trending .colorstripe li a {
    font-size: .9rem;
  }
}

.e-trending .colorstripe li.all {
  background: #383b43;
}

.e-trending .colorstripe li.all a {
  background: #383b43;
}

.e-trending .colorstripe li.all:hover a {
  background: #282830;
}

.e-trending .colorstripe li.comedy {
  background: #f19144;
}

.e-trending .colorstripe li.comedy a {
  background: #f19144;
}

.e-trending .colorstripe li.comedy:hover a {
  background: #c66218;
}

.e-trending .colorstripe li.crime {
  background: #ea6c67;
}

.e-trending .colorstripe li.crime a {
  background: #ea6c67;
}

.e-trending .colorstripe li.crime:hover a {
  background: #b6312a;
}

.e-trending .colorstripe li.reality {
  background: #70607e;
}

.e-trending .colorstripe li.reality a {
  background: #70607e;
}

.e-trending .colorstripe li.reality:hover a {
  background: #47345d;
}

.e-trending .colorstripe li.action {
  background: #70607e;
}

.e-trending .colorstripe li.action a {
  background: #70607e;
}

.e-trending .colorstripe li.action:hover a {
  background: #47345d;
}

.e-trending .colorstripe li.drama {
  background: #55abbd;
}

.e-trending .colorstripe li.drama a {
  background: #55abbd;
}

.e-trending .colorstripe li.drama:hover a {
  background: #3f6381;
}

.e-trending .colorstripe li.sci-fi {
  background: #adc26d;
}

.e-trending .colorstripe li.sci-fi a {
  background: #adc26d;
}

.e-trending .colorstripe li.sci-fi:hover a {
  background: #869928;
}

.e-trending .colorstripe li.active.all {
  background: #282830;
}

.e-trending .colorstripe li.active.comedy {
  background: #c66218;
}

.e-trending .colorstripe li.active.crime {
  background: #b6312a;
}

.e-trending .colorstripe li.active.reality {
  background: #383b43;
}

.e-trending .colorstripe li.active.action {
  background: #383b43;
}

.e-trending .colorstripe li.active.drama {
  background: #3f6381;
}

.e-trending .colorstripe li.active.sci-fi {
  background: #869928;
}

.e-trending .colorstripe li:hover a {
  color: #fff;
}

.e-trending .comedy:not(.filtered):hover {
  background: #f19144;
  color: #fff;
}

.e-trending .comedy .bubble a.view-show:hover {
  background: #c66218;
}

.e-trending .crime:not(.filtered):hover {
  background: #ea6c67;
  color: #fff;
}

.e-trending .crime .bubble a.view-show:hover {
  background: #b6312a;
}

.e-trending .reality:not(.filtered):hover {
  background: #70607e;
  color: #fff;
}

.e-trending .reality .bubble a.view-show:hover {
  background: #47345d;
}

.e-trending .action:not(.filtered):hover {
  background: #70607e;
  color: #fff;
}

.e-trending .action .bubble a.view-show:hover {
  background: #47345d;
}

.e-trending .drama:not(.filtered):hover {
  background: #55abbd;
  color: #fff;
}

.e-trending .drama .bubble a.view-show:hover {
  background: #3f6381;
}

.e-trending .sci-fi:not(.filtered):hover {
  background: #adc26d;
  color: #fff;
}

.e-trending .sci-fi .bubble a.view-show:hover {
  background: #869928;
}

.e-trending [class*="genre-"] {
  display: block;
  position: absolute;
  bottom: 0;
  left: 0;
  color: #fff;
  background-color: #999;
  width: 100%;
  text-align: center;
  text-transform: uppercase;
  font-size: 11px;
  font-weight: bold;
}

.e-trending .genre-sci-fi {
  background-color: #869928;
}

.e-trending .genre-crime {
  background-color: #ea6c67;
}

.e-trending .genre-comedy {
  background-color: #f19144;
}

.e-trending .genre-reality {
  background-color: #70607e;
}

.e-trending .genre-action {
  background-color: #70607e;
}

.e-trending .genre-drama {
  background-color: #55abbd;
}

.e-trending .row.colorstripe {
  list-style: none;
  margin: 50px 0;
}

.e-trending .buttons li {
  border-left: 1px solid #fff;
  border-right: 1px solid #fff;
}

.e-trending .buttons li a.all {
  background: #383b43;
  color: #fff;
}

.e-trending .buttons li a.all:visited {
  background: #383b43;
  color: #fff;
}

.e-trending .buttons li a.all:hover {
  background: #282830;
}

.e-trending .buttons li a.all.active {
  background: #282830;
}

.e-trending .buttons li a.comedy {
  background: #f19144;
  color: #fff;
}

.e-trending .buttons li a.comedy:visited {
  background: #f19144;
  color: #fff;
}

.e-trending .buttons li a.comedy:hover {
  background: #c66218;
}

.e-trending .buttons li a.comedy.active {
  background: #c66218;
}

.e-trending .buttons li a.crime {
  background: #ea6c67;
  color: #fff;
}

.e-trending .buttons li a.crime:visited {
  background: #ea6c67;
  color: #fff;
}

.e-trending .buttons li a.crime:hover {
  background: #b6312a;
}

.e-trending .buttons li a.crime.active {
  background: #b6312a;
}

.e-trending .buttons li a.reality {
  background: #70607e;
  color: #fff;
}

.e-trending .buttons li a.reality:visited {
  background: #70607e;
  color: #fff;
}

.e-trending .buttons li a.reality:hover {
  background: #47345d;
}

.e-trending .buttons li a.reality.active {
  background: #47345d;
}

.e-trending .buttons li a.action {
  background: #70607e;
  color: #fff;
}

.e-trending .buttons li a.action:visited {
  background: #70607e;
  color: #fff;
}

.e-trending .buttons li a.action:hover {
  background: #47345d;
}

.e-trending .buttons li a.action.active {
  background: #47345d;
}

.e-trending .buttons li a.drama {
  background: #55abbd;
  color: #fff;
}

.e-trending .buttons li a.drama:visited {
  background: #55abbd;
  color: #fff;
}

.e-trending .buttons li a.drama:hover {
  background: #3f6381;
}

.e-trending .buttons li a.drama.active {
  background: #3f6381;
}

.e-trending .buttons li a.sci-fi {
  background: #adc26d;
  color: #fff;
}

.e-trending .buttons li a.sci-fi:visited {
  background: #adc26d;
  color: #fff;
}

.e-trending .buttons li a.sci-fi:hover {
  background: #869928;
}

.e-trending .buttons li a.sci-fi.active {
  background: #869928;
}

.e-trending .all:not(.filtered):hover {
  background: #383b43;
  color: #fff;
}


/* Awards Show Pages */

.e-awards-shows.little-pic {
  float: left;
  margin-right: 20px;
  width: 20%;
}

.e-awards-shows .module-header h2 {
  width: 100%;
}

.e-awards-shows .nominee {
  padding: 0 5px 5px 0 !important;
  text-align: center;
}

@media only screen and (max-width: 47.937em) {
  .e-awards-shows .nominee {
    text-align: left;
  }
}

.e-awards-shows a.nominee-pic {
  margin-bottom: 5px;
}

.e-awards-shows ul {
  padding-left: 10px;
}

.e-awards-shows li {
  position: relative;
}

.e-awards-shows .nominee-pic img {
  width: 100%;
}

.e-awards-shows .title {
  text-align: center;
  margin-top: 5px;
  display: block;
}

@media only screen and (max-width: 47.937em) {
  .e-awards-shows .title {
    text-align: left;
    margin-top: 0;
  }
}

.e-awards-shows .winner.row.collapse {
  float: right;
  width: 75%;
}

.e-awards-shows .winner .title {
  text-align: center;
}

.e-awards-shows .winner img {
  border: solid 3px #55abca;
  padding: 0;
}

.e-awards-shows img.interview {
  z-index: 1;
  width: 35%;
  position: absolute;
  left: 55%;
  top: 2%;
}

.e-awards-shows img.last-winner {
  z-index: 2;
  position: absolute;
  left: -1px;
  top: -1px;
  width: 50%;
}

.e-awards-shows p {
  color: white;
  font-weight: bold;
  display: block;
  background-color: #55abca;
  text-align: center;
  padding-bottom: 2px;
  margin-bottom: 0;
}


/* Slideshow */

.e-slideshow a {
  color: white;
}

.e-slideshow img.cb {
  width: 70%;
}

.e-slideshow p.line1-flag {
  margin-bottom: 0;
}

.e-slideshow .celebified-line1 h2 {
  background-color: #ea0048;
  padding: 7px;
  color: white;
  margin-top: 5px;
}

.e-slideshow .celebified-line1 .button {
  margin-top: 7px;
}

@media only screen and (max-width: 47.937em) {
  .e-slideshow img.cb {
    width: 33%;
    float: left;
  }
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-slideshow img.cb {
    height: 48px;
    width: auto;
    float: left;
    margin-top: 5px;
  }
}

@media only screen and (max-width: 67.938em) {
  .e-slideshow .line1-flag .flag {
    line-height: 1.5em;
  }
  .e-slideshow .line1-flag .flag::after {
    right: -0.75em;
    border-right-width: 0.75em;
    border-top-width: 0.75em;
    border-bottom-width: 0.75em;
  }
  .e-slideshow p.episode-title {
    margin-bottom: 0.1rem;
  }
}


/* Trending Shows Sidebar */

.e-trending-sidebar h2 {
  width: 100%;
  text-align: center;
}

.e-trending-sidebar img {
  width: 69px;
  height: 30px;
}


/*Top Shows by State/Country */
.e-top-shows-map.totalMap {
  width: 100%;
  padding: 10px;
  color: white;
  background-color: #0080ae;
}

.e-top-shows-map.totalMap p {
  font-size: 13px;
  margin-bottom: 0;
  font-family: Montserrat;
}

.e-top-shows-map.totalMap .click {
  font-size: 15px;
  font-weight: bold;
}

.e-top-shows-map #instructionsInner {
  padding: 10px 0 0 10px;
  position: absolute;
  z-index: 1;
}

.e-top-shows-map #instructionsInner h1 {
  float: left;
  font-size: 30px;
  position: relative;
}

.e-top-shows-map #instructionsInner img {
  padding: 13px 0 0 10px;
  float: right;
}

.e-top-shows-map #vmap {
  width: 100%;
  height: 500px;
  position: relative;
  overflow: hidden;
  background-color: rgb(167, 211, 234);
}

.e-top-shows-map.stateShows {
  width: 100%;
  background-color: white;
  border-top-style: solid;
  border-color: #0080ae;
  border-width: 7px;
  display: none;
  margin-bottom: 10px;
}

.e-top-shows-map.stateShows .header {
  width: 100%;
  background-color: #55abca;
  padding: 10px 10px 0 10px;
  margin-bottom: 5px;
  background-repeat: no-repeat;
  background-position: right 10px top 7px;
  background-size: auto 92%;
}

.e-top-shows-map .state-name {
  position: relative;
  height: inherit;
}

.e-top-shows-map .state-name h1 {
  color: white;
  margin-bottom: 0px;
  font-size: 35px;
  padding-bottom: 0px;
  text-align: right;
}

.e-top-shows-map .first h1 {
  font-size: 102px;
  color: #a7d3ea;
  margin-bottom: 0;
  position: relative;
  top: -11px;
  right: -7px;
  line-height: 96px;
}

.e-top-shows-map .first h2 {
  color: white;
  text-align: left;
  position: relative;
  bottom: 10px;
  line-height: 21px;
  font-size: 25px;
  right: -9px;
}

.e-top-shows-map .first h2 a {
  color: white;
}

.e-top-shows-map .poster {
  position: relative;
  float: left;
  margin-bottom: 12px;
  border: solid 1px #a7d3ea;
}

.e-top-shows-map .poster img {
  border: solid 1px #a7d3ea;
  margin-bottom: 0;
  width: 50%;
}

.e-top-shows-map .number {
  margin: 10px 0;
  position: relative;
  float: left;
}

.e-top-shows-map .number .circle {
  height: 40px;
  width: 40px;
  background: #808285;
  -moz-border-radius: 20px;
  -webkit-border-radius: 20px;
  position: relative;
  float: left;
  margin-left: 10px;
}

.e-top-shows-map .number .circle h3 {
  color: white;
  font-size: 33px;
  text-align: center;
  line-height: 38px;
}

.e-top-shows-map .number p {
  height: 40px;
  line-height: 40px;
  margin: 0 0 0 55px;
}

.e-top-shows-map .number p a {
  font-size: 16px;
  display: inline-block;
  vertical-align: middle;
  line-height: normal;
}

@media only screen and (max-width: 47.937em) {
  .e-top-shows-map.totalMap {
    background-color: #a7d3ea;
    color: #505050;
  }
  .e-top-shows-map.totalMap p {
    z-index: 5;
  }
  .e-top-shows-map.totalMap .click {
    font-weight: normal;
  }
  #instructionsInner {
    display: none;
  }
  .e-top-shows-map.map-L1 {
    height: 250px;
  }
  .e-top-shows-map #vmap {
    height: 360px;
    top: -60px;
    width: 100%;
  }
  .e-top-shows-map.stateShows .header {
    background-color: #a7d3ea;
  }
  .e-top-shows-map.stateShows .header .poster img {
    border-color: #0080ae;
  }
  .e-top-shows-map.stateShows .first h1 {
    font-size: 80px;
    line-height: 1;
    color: #55abca;
  }
  .e-top-shows-map .poster {
    border: solid 1px #0080ae;
  }
  .e-top-shows-map .poster img {
    border: solid 1px #0080ae;
  }
}

.e-top-shows-map.partial-bordered-module {
  padding: 0;
}

.e-top-shows-map.partial-bordered-module p {
  padding: 10px 10px 0 10px;
}

.e-top-shows-map .sidebarheader {
  background-color: #0080ae;
  padding: 10px;
}

.e-top-shows-map .sidebarheader h2 {
  color: white;
}

.e-top-shows-map .sidebarContent {
  background-color: #55abca;
  height: 128px;
  width: 100%;
}

.e-top-shows-map .firstShow {
  width: 201px;
  position: relative;
  float: left;
}

.e-top-shows-map .sidebarContent h1 {
  font-size: 102px;
  color: #a7d3ea;
  text-align: right;
  margin-bottom: 0;
  position: absolute;
  right: -9px;
  top: -17px;
}

.e-top-shows-map .sidebarContent h2 {
  text-align: right;
  margin-bottom: 0;
  position: absolute;
  right: -1px;
  bottom: -124px;
  font-size: 21px;
}

.e-top-shows-map .sidebarContent h2 a {
  color: white;
}

.e-top-shows-map .sidebarContent .poster {
  margin: 10px;
  position: relative;
  float: right;
}

.e-top-shows-map .sidebarContentShows {
  width: 300px;
  background-color: white;
  height: 375px;
}

.e-top-shows-map .sidebarContentShows p {
  line-height: 40px;
  margin: 3px 0 0 40px;
  width: 240px;
  padding: 0;
}

.e-top-shows-map .sidebarContentShows p a {
  display: inline-block;
  vertical-align: middle;
  line-height: normal;
  margin-top: 6px;
}

.e-top-shows-map .sidebarContentShows .number {
  margin: 10px 0 0 0;
  position: relative;
  float: left;
  width: 290px;
}

.e-top-shows-map .sidebarContentShows .number .circle {
  height: 30px;
  width: 30px;
  -moz-border-radius: 15px;
  -webkit-border-radius: 15px;
}

.e-top-shows-map .sidebarContentShows .number .circle h3 {
  color: white;
  font-size: 26px;
  text-align: center;
  line-height: 27px;
}

.e-top-shows-map .sidebarContentShows .number p {
  width: 290px;
  height: 20px;
  line-height: 20px;
  margin: 0 0 0 45px;
}

.e-top-shows-map .sidebarPoster {
  margin: 10px 10px 0 0;
  position: relative;
  float: right;
}

.e-top-shows-map .sidebarPoster img {
  border-style: solid;
  border-width: 2px;
  border-color: #a7d3ea;
  width: 75px;
  height: 109px;
}

.e-top-shows-map .fb_iframe_widget,
.e-top-shows-map .fb_iframe_widget span,
.e-top-shows-map .fb_iframe_widget span iframe[style] {
  min-width: 100% !important;
  width: 100% !important;
}


/* End of Season Reaction Pages */

.e-reactions.editor {
  position: relative;
  padding-top: 50px;
}

.e-reactions.editor .row {
  position: absolute;
  z-index: 2;
  width: 100%;
}

.e-reactions.editor .editor-image img {
  width: 70%;
  border: 4px solid #6fa049;
  float: right;
}

.e-reactions.editor .editor-image.contributor img {
  border: 4px solid #78698f;
}

.e-reactions.editor .editor-title {
  position: relative;
  height: 50px;
}

.e-reactions.editor .editor-title h2 {
  padding-left: 5px;
  position: absolute;
  bottom: 0;
}

.e-reactions.partial-bordered-module {
  padding: 0;
}

ul.e-reactions {
  position: relative;
  top: 50px;
  margin-bottom: 9px;
  overflow: hidden;
}

.e-reactions li {
  padding: 0 0 1000px 0;
  margin-bottom: -1000px;
}

.e-reactions .cancel {
  background-color: #F0BFBC;
}

.e-reactions .renew {
  background-color: #D4E5C7;
}

.e-reactions .cancel .line1 hr {
  border: 1px solid #E58F8B;
  margin: 7px 0 0 0;
}

.e-reactions .cancel .line2 hr {
  border: 1px solid #E17F7A;
  margin: 4px 0 0 0;
}

.e-reactions .cancel .line3 hr {
  border: 1px solid #DD6F69;
  margin: 4px 0 10px 0;
}

.e-reactions .renew .line1 hr {
  border: 1px solid #B4D29D;
  margin: 7px 0 0 0;
}

.e-reactions .renew .line2 hr {
  border: 1px solid #A9CC8F;
  margin: 4px 0 0 0;
}

.e-reactions .renew .line3 hr {
  border: 1px solid #9FC581;
  margin: 4px 0 10px 0;
}

.e-reactions .cancel img {
  border-color: #b6312a;
}

.e-reactions .renew img {
  border-color: #6fa049;
}

.e-reactions .reaction {
  padding: 5px;
}

.e-reactions .reaction img {
  border-style: solid;
  border-width: 2px;
  width: 33%;
  vertical-align: top;
}

.e-reactions .reaction h3 {
  margin: 0 0 3px 0;
  position: relative;
  top: -2px;
  line-height: 1rem;
}

.e-reactions .reaction a {
  width: 64%;
  display: inline-block;
}

.e-reactions .reaction p {
  font-style: italic;
  margin-top: 3px;
  margin-bottom: 0;
}

@media only screen and (max-width: 47.937em) {
  .e-reactions.editor .editor-image img {
    width: 90%;
  }
  ul.e-reactions {
    border-left: 0;
    border-right: 0;
  }
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-reactions.editor .editor-image img {
    position: relative;
    bottom: -9px;
  }
  .e-reactions .reaction a {
    font-size: .9rem;
  }
}

@media only screen and (min-width: 68em) {
  .e-reactions.partial-bordered-module {
    padding-bottom: 5px;
  }
  .e-reactions .reaction img {
    float: right;
    margin-left: 5px;
  }
}


/* Infographics Module */

.e-infographics img {
  width: 100%;
  margin-bottom: 5px;
}

.e-infographics .small-12.medium-6.large-4 {
  float: left;
  padding: 10px;
}


/* Support SideReel */
.e-support img.google-contributor {
  width: 75%;
  border: 1px solid #cecfd0; 
  margin: 0 auto 10px auto;
  display: block;
}
.e-support img.intro {
  width: 15%;
  float: right;
  margin-left: 10px;
}
.e-support .store {
  width: 75%;
  float: left;
}
.e-support img.store {
  width: 25%;
  float: right;
}
@media only screen and (max-width: 47.937em) {
  .e-support .store {
    width: 100%;
    background-image: url(http://editorial.sidereel.com/responsive/images/support-sidereel/tote.png);
    background-position: right;
    background-size: 39%;
    background-repeat: no-repeat;
    float: none;
  }
  .e-support .store p {
    width: 50%;
  }
  .e-support img.store {
    width: 50%;
  }
  .e-support img.intro {
    width: 35%;
  }
}


/* Best/Worst Shows of the Year */

img.e-best-worst {
  width: 100%;
}

.e-best-worst.partial-bordered-module.row {
  margin-bottom: 10px;
}

.e-best-worst .module-header img {
  width: 7%;
  border-style: solid;
  border-width: 2px;
  margin-bottom: 9px;
  margin-right: 5px;
}

.e-best-worst .editor {
  border-color: #6fa049;
}

.e-best-worst .contributor {
  border-color: #78698f;
}

.e-best-worst .small-12.medium-6 {
  padding-left: 0;
  padding-right: 0;
}

.e-best-worst img.small-4.medium-3 {
  float: right;
  padding-left: 0;
}

.e-best-worst ul {
  list-style-type: none;
  margin: 0;
  padding-bottom: 5px;
}

.e-best-worst .end li {
  margin-bottom: 4px;
}

.e-best-worst .best {
  border-right: 1px solid #cacbcd;
}

@media only screen and (max-width: 47.937em) {
  .e-best-worst.partial-bordered-module.row {
    margin: 0 10px 10px 10px;
  }
  .e-best-worst.partial-bordered-module {
    margin: 0 10px 10px 10px;
  }
  .e-best-worst .best {
    border-right: none;
    border-bottom: 1px solid #cacbcd;
    padding-bottom: 10px;
    margin-bottom: 10px;
  }
  .e-best-worst .module-header img {
    width: 20%;
  }
}


/* Anonymous Recommendations */

.e-anonymous-recs > .row {
  margin-top: 10px;
  margin-bottom: 15px;
  padding-bottom: 5px;
}

.e-anonymous-recs > .row:not(:last-child) {
  border-bottom: 1px solid #dddfdd;
}

.e-anonymous-recs img {
  border: 1px solid #55ABCA;
}

@media only screen and (min-width: 68em) {
  .e-anonymous-recs .profile img {
    margin-top: 25px;
  }
}

.e-anonymous-recs .activity p {
  margin-left: 0.75rem;
}

.e-anonymous-recs .activity ul {
  list-style-type: none;
}

.e-anonymous-recs .activity .icon {
  color: #f19144;
}

.e-anonymous-recs .activity-icon {
  border: none;
  position: absolute;
  top: 110px;
  right: 11px;
}

.e-anonymous-recs .arrow img {
  border: none;
  padding-top: 62px;
}

@media only screen and (min-width: 47.938em) and (max-width: 67.938em) {
  .e-anonymous-recs .recommendation {
    padding-left: 0;
    margin-bottom: 10px;
  }
  .e-anonymous-recs .recommendation p {
    margin-bottom: 0;
  }
}

.e-anonymous-recs .recommendation .activity-icon {
  top: 145px;
}


/* Cord-Cutting Guide */

.e-cord-cutting h5 {
  font-size: 1rem;
}

.e-cord-cutting .nav-item-container {
  padding: 0;
}

.e-cord-cutting .nav-item-container.for-tv-pages {
  margin-top: 5px;
  margin-bottom: 0px;
}

.e-cord-cutting .cost-quality {
  background-color: #EFEEEB;
}

.e-cord-cutting .cost-quality .profile-jess {
  background-color: white;
}

.e-cord-cutting .cost-quality .person-info {
  padding-top: 10px;
}

@media only screen and (max-width: 47.937em) {
  .e-cord-cutting .cost-quality .person-info img {
    padding-right: 10px !important;
  }
}

.e-cord-cutting .cost-quality .person-info h3 {
  font-size: 15px;
}

.e-cord-cutting .cost-quality .table-header {
  background: #0080AE;
  text-align: center;
  z-index: 100;
}

.e-cord-cutting .cost-quality .table-header h2 {
  color: white;
  line-height: 2em;
}

.e-cord-cutting .rec-hardware {
  margin-top: 5px;
}

.e-cord-cutting ul {
  margin: 1rem 0.5rem 1rem 1.5rem;
}

.e-cord-cutting ul li {
  margin-bottom: 0.5rem;
}

.e-cord-cutting > p {
  font-size: 14px;
}

.e-cord-cutting table {
  width: 100%;
}

.e-cord-cutting table td {
  text-align: center;
}

.e-cord-cutting .item {
  border-top: 1px solid #dddfdd;
  padding: 10px 0 5px 0;
  margin-top: 15px;
}

.e-cord-cutting .item p {
  font-size: 14px;
  text-align: justify;
}

.e-cord-cutting .item .amazon-link {
  background-color: #FF960B;
}

.e-cord-cutting .item .amazon-link:hover {
  background-color: #E77600;
}

.e-cord-cutting .item .back-to-chart {
  font-style: italic;
  margin-top: 1rem;
}

.e-cord-cutting .item .pros {
  color: #6FA049;
}

.e-cord-cutting .item .cons {
  color: #B6312A;
}

.e-cord-cutting .back-to-home {
  padding-top: 20px;
  margin-top: 10px;
  border-top: 1px solid #dddfdd;
}



/*# sourceMappingURL=editorial.css.map */
