#!/bin/bash

# Script to filter a list of files based on their size.
# Any file with size 400 bytes or less is removed.

CSS_LIST=random-files.txt

while read f ; do
   minSize=400
   size=$(wc -c $f | awk '{print $1;}')
   if [ $size -gt $minSize ] ; then
	   echo $f
   fi
done <$CSS_LIST
