#!/bin/bash
 
# FILES=sheets/*
# FILES=900-1000/*
CSS_LIST=final_list.txt
hasminOptions="--no-lowercasing --no-selector-sorting --no-property-sorting --no-quote-normalization -a"
minifier=( hasmin cssnano cleancss csso )
minifierSeq=$(seq 0 $((${#minifier[@]} - 1)))

# Set up directories
for i in $minifierSeq
do
  dir[i]=${minifier[i]}-output
  rm -rf ${dir[i]}
  mkdir ${dir[i]}
done

# Setup file to store list of those that were accepted by the four minifiers (and passed 7z).
# rm working-files.txt
# touch working-files.txt

count=0
while read f ; do
  # take action on each file. $f stores current file name
  echo "Processing $f file ($count)..."
  let count=count+1

  ../.stack-work/install/12018b8a/bin/hasmin.exe ${hasminOptions} $f > ${dir[0]}/$(basename $f)
  e1=$? ; if [[ "$e1" -ne "0" ]] ; then continue; fi
  7z a -tgzip ${dir[0]}/$(basename $f).gz ${dir[0]}/$(basename $f) -mx=9 > /dev/null
  e2=$?;  if [[ "$e2" -ne "0" ]] ; then continue; fi

  powershell cssnano --no-discardUnused $f > ${dir[1]}/$(basename $f)
  e3=$?;  if [[ "$e3" -ne "0" ]] ; then continue; fi
  7z a -tgzip ${dir[1]}/$(basename $f).gz ${dir[1]}/$(basename $f) -mx=9 > /dev/null
  e4=$?;  if [[ "$e4" -ne "0" ]] ; then continue; fi

  powershell cleancss --skip-import $f > ${dir[2]}/$(basename $f)
  e5=$?;  if [[ "$e5" -ne "0" ]] ; then continue; fi
  7z a -tgzip ${dir[2]}/$(basename $f).gz ${dir[2]}/$(basename $f) -mx=9 > /dev/null
  e6=$?;  if [[ "$e6" -ne "0" ]] ; then continue; fi

  powershell csso $f > ${dir[3]}/$(basename $f)
  e7=$?;  if [[ "$e7" -ne "0" ]] ; then continue; fi
  7z a -tgzip ${dir[3]}/$(basename $f).gz ${dir[3]}/$(basename $f) -mx=9 > /dev/null
  e8=$?;  if [[ "$e8" -ne "0" ]] ; then continue; fi

  # Uncomment to store list of working files.
  # echo $f >> working-files.txt
done <$CSS_LIST

for i in $minifierSeq
do
  result[i]=${minifier[i]}-results.csv

  # Get size data and store it in a csv file
  # 1. Remove first line
  # 2. $6 = bytes, $10 = names
  # 3. Put names first, separate them with a semicolon
  # 4. Every second line (those with .gz files) print argument $0 and append a semicolon
  # 5. Using a semicolon as field separator (-F';') ...
  ls ${dir[i]} -l \
     | awk '{if(NR>1)print}' \
     | awk '{print $6"  "$10}' \
     | awk '{print $2 ";" $1}' \
     | awk 'NR%2{printf "%s;",$0;next;}1' \
     | awk -F';' '{print $1"; "$2"; "$4}' > ${result[i]}
done

# Combine results files to produce final csv file
paste --delimiter=' ' ${result[0]} ${result[1]} ${result[2]} ${result[3]} \
  | awk '{print $1 $2 $5 $8 $11 $3";"$6";"$9";"$12}' \
  | sed -e '1iFile name; Hasmin; cssnano; cleancss; csso; hasmin+gzip; cssnano+gzip; cleancss+gzip; csso+gzip' > final.csv
