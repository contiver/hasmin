#!/bin/bash
 
# Script to crawl Alexa's top 10000 websites, download their CSS, and only keep
# it if it is not minified (technically, only if it doesn't contain a semicolon
# at the end of a line, which probably means the file isn't minified).

filename=../top10000.txt
count=1
while read p; do
  echo "Processing site $count: $p"
  let count=count+1

  # Crawl CSS, trying 3 times per link, with a 15 second timeout.
  wget -E -H -K -A css -t 3 -T 15 -p --quiet $p

  find . -type f -iname "*.css" -print0 | while IFS= read -r -d $'\0' line; do
    if grep -r -l ';$' $line; then # if the file does not contain a line ending in ';'
      mkdir -p ../dataset/${p%?}
      cp $line ../dataset/${p%?}/
    fi
  done
  rm -R `ls -1 -d */`
done <$filename
