#!/bin/bash

# Script for compressing all the files listed in a file (CSS_LIST), and store
# them in a directory.

CSS_LIST=final_list.txt

# Set up 7z only directory
rm -rf 7z
mkdir 7z

count=1
while read f ; do
  echo "Processing $f file ($count)..."
  let count=count+1
  7z a -tgzip 7z/$(basename $f).gz $f -mx=9 > /dev/null
done <$CSS_LIST
