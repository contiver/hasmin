Hasmin - A Haskell CSS minifier
====

Hasmin is a CSS minifier written in Haskell for the Master's Thesis
*Minification and Compression of CSS Files*. For more details than those
present in this readme, refer to the thesis.

Aside from the usual techniques (e.g. whitespace removal, color minification,
etc.), the idea was to explore new possibilities, by implementing things
minifiers weren't doing (for example, property traits), or they were, but not
taking full advantage of them (see gradient and transform-function
minification).

Also, the minifier implements some techniques that do nothing for minified
sizes, but attempt to improve post-compression sizes (at least when using
DEFLATE, i.e. gzip).

The whole list of optimizations implemented can be found in the thesis.

### Building 
To compile, just run `stack build`.

### Minifier Usage
Every technique is enabled by default. When something needs to be disabled, use
the appropriate flag. Not every technique can be toggled, but a good amount of
them allow it.

Hasmin expects a path to the CSS file, and outputs the minified result to
stdout.

Note: there is a problem in Windows when not using the `-u` flag to disable the
conversion of escaped characters. A workaround is changing the code page, which
can be done by running `chcp 65001` in the terminal (whether native, or
cygwin).

### Data and Experiments
The crawled CSS data, and scripts used are found in the benchmarks directory.
Scripts begin with a short commentary explaining their purpose. Note that
development was done on a Windows environment, and the scripts reflect that. To
run them on Linux or iOS, some of them will need modifications (particularly
any powershell call).

The **sheets** directory contains style sheets taken from GitHub repositories
(mainly frameworks), which were used for a first set of tests.

The **wget** directory contains the script used for crawling the websites, and is
used by the script as a temporary directory to store the crawled files, and
decide if they should be kept or deleted (if they are already minified).

The **no-dups-dataset** directory contains the files left after using `fdupes` on
the set of files crawled to remove duplicates.

Lastly, the **final_list.txt** lists the files used for the second set of
tests, after randomly picking 1000, and filtering those of small size or that
made a minifier fail.

## Zopfli integration
Hasmin gives the possibility to use google's
[zopfli algorithm](https://en.wikipedia.org/wiki/Zopfli), to compress the
result. Since it is a DEFLATE implementation, it is fully compatible with gzip.
It tipically produces files 3~8% smaller than zlib, at the cost of being around
80 times slower, so it is only a good idea if you don't need compression on the fly.


Zopfli is released under the Apache License, Version 2.0.
