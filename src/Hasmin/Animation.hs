{-# LANGUAGE OverloadedStrings #-}

module Hasmin.Animation where

import Control.Monad.Reader
import Data.Semigroup ((<>))
--import Data.Text (Text, toCaseFold)
import Prelude hiding (sin, cos, acos, tan, atan)
import Data.Maybe (isNothing, fromJust)
import Data.Text.Lazy.Builder (singleton)
import Hasmin.Config
import Hasmin.Types.Class
import Hasmin.Utils
import Hasmin.Types.Numeric

-- <single-animation-iteration-count> = infinite | <number>
data AnimationIterationCount = Infinite | AICount Number
  deriving (Eq, Show)

instance ToText AnimationIterationCount where
  toBuilder Infinite    = "infinite"
  toBuilder (AICount n) = toBuilder n

-- <single-animation-direction> = normal | reverse | alternate | alternate-reverse
data AnimationDirection = Normal | Reverse | Alternate | AlternateReverse
  deriving (Eq, Show)

instance ToText AnimationDirection where
  toBuilder Normal           = "normal"
  toBuilder Reverse          = "reverse"
  toBuilder Alternate        = "alternate"
  toBuilder AlternateReverse = "alternateReverse"

-- <single-animation-fill-mode> = none | forwards | backwards | both
data SingleAnimationFillMode = None | Forwards | Backwards | Both
  deriving (Eq, Show)

-- <single-animation-play-state> = running | paused
data SingleAnimationPlayState = Running | Paused
  deriving (Eq, Show)                            

-- <keyframes-name> = <custom-ident> | <string>
