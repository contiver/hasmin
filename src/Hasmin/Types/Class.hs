module Hasmin.Types.Class where

import Control.Monad.Reader
import Data.Word (Word8)
import Data.Text (pack, Text)
import Data.Text.Lazy (toStrict)
import Data.Text.Lazy.Builder (Builder, fromText, toLazyText)
import Hasmin.Config

-- | Class for values that can be minified
class Minifiable a where 
  {-# MINIMAL minifyWith #-}
  minifyWith :: a -> Reader Config a
  minify :: a -> a -- returns smallest equivalent representation for an element
  minify x = runReader (minifyWith x) defaultConfig

class ToText a where
  -- {-# MINIMAL toText | toBuilder #-}
  toText :: a -> Text
  toBuilder :: a -> Builder
  toText    = toStrict . toLazyText . toBuilder
  toBuilder = fromText . toText
instance ToText Word8 where
  toText = pack . show
instance ToText Int where
  toText = pack . show
instance ToText Text where
  toText = id
instance (ToText a, ToText b) => ToText (Either a b) where
  toText = either toText toText
  toBuilder = either toBuilder toBuilder

-- | Class of shorthand properties that can be expanded to longhand equivalents
-- class Shorthand a where 
  -- expand :: a -> [a]
