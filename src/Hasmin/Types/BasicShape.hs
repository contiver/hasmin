{-# LANGUAGE OverloadedStrings #-}

module Hasmin.Types.BasicShape where

import Data.Semigroup ((<>))
--import Data.Text (Text, toCaseFold)
import Data.Text.Lazy (toStrict)
import qualified Data.Text as T
import Data.Number.FixedFunctions (sin, cos, acos, tan, atan)
import Prelude hiding (sin, cos, acos, tan, atan)
import qualified Data.Matrix as M
import Data.Matrix (Matrix) 
import Data.Maybe (catMaybes, isNothing, isJust, fromJust)
import Data.Text.Lazy.Builder (toLazyText, singleton, Builder)
--import Hasmin.Arguments
import Hasmin.Types.Class
import Hasmin.Utils
import Hasmin.Types.Dimension
import Hasmin.Types.Numeric
--import Text.PrettyPrint.Mainland (Pretty, ppr, strictText)

-- | CSS \<basic-shapes\> data type. Specification:
--
-- 1. https://drafts.csswg.org/css-shapes/#basic-shape-functions
data BasicShape = Inset
                | Circle 
                | Ellipse
                | Polygon
