{-# LANGUAGE OverloadedStrings #-}
module Hasmin.Types.Margin where

{-
import Hasmin.Utils
import Hasmin.Types.Numeric
import Hasmin.Types.Dimension
import Hasmin.Types.Class
import Data.Text (Text, pack)

data MarginTop = MarginTop
data MarginBottom = MarginBottom
data MarginRight = MarginRight
data MarginLeft = MarginLeft

type PropertyName = Text
data MarginSide = MarginSide PropertyName (Either Csswide MarginWidth)
  deriving (Show)
instance Minifiable MarginSide where
  minRep (MarginSide n (Right x)) = MarginSide n (Right (minRep x))
  minRep r                        = r
  
  minShow (Mlength d)     = minShow d
  minShow (Mpercentage p) = minShow p
  minShow Mauto           = pack $ show Mauto

instance Auto MarginWidth where
  auto = Mauto

-- <margin-width> value type
data MarginWidth = Mlength Distance
                 | Mpercentage Percentage
                 | Mauto
instance Show MarginWidth where
  show Mauto           = "auto"
  show (Mpercentage p) = show p
  show (Mlength l)     = show l
instance Minifiable MarginWidth where
  minRep (Mlength d) = Mlength (minRep d)
  minRep x           = x
  
  minShow (Mlength d)     = minShow d
  minShow (Mpercentage p) = minShow p
  minShow Mauto           = pack $ show Mauto

instance Auto MarginWidth where
  auto = Mauto

-- margin-top property
data MarginTopP = MarginTopP MarginWidth
                | MTkeyword Csswide
instance Show MarginTopP where
  show (MTkeyword k) = show k
  show (MarginTopP w) = mconcat ["margin-top: ", show w]
instance Property MarginTopP where
  initial = (MTkeyword Initial)
  inherit = (MTkeyword Inherit)
  unset   = (MTkeyword Unset)
instance Minifiable MarginTopP where
  minRep (MarginTopP w) = MarginTopP (minRep w)
  minRep x              = x

  minShow (MarginTopP w) = minShow w
  minShow x              = pack $ show x

data MarginRightP = MarginRightP MarginWidth
                  | MRkeyword Csswide
instance Show MarginRightP where
  show (MRkeyword k)    = show k
  show (MarginRightP w) = show w
instance Property MarginRightP where
  initial = MRkeyword Initial
  inherit = MRkeyword Inherit
  unset   = MRkeyword Unset
instance Minifiable MarginRightP where
  minRep (MarginRightP w) = MarginRightP (minRep w)
  minRep x                = x

  minShow (MarginRightP w) = minShow w
  minShow x                = pack $ show x

data MarginBottomP = MarginBottomP MarginWidth
                   | MBkeyword Csswide
instance Show MarginBottomP where
  show (MBkeyword k)     = show k
  show (MarginBottomP w) = show w
instance Property MarginBottomP where
  initial = MBkeyword Initial
  inherit = MBkeyword Inherit
  unset   = MBkeyword Unset
instance Minifiable MarginBottomP where
  minRep (MarginBottomP w) = MarginBottomP (minRep w)
  minRep x                 = x

  minShow (MarginBottomP w) = minShow w
  minShow x                 = pack $ show x

data MarginLeftP = MarginLeftP MarginWidth
                 | MLkeyword Csswide
instance Show MarginLeftP where
  show (MLkeyword k)  = show k
  show (MarginLeftP w) = show w
instance Property MarginLeftP where
  initial = MLkeyword Initial
  inherit = MLkeyword Inherit
  unset   = MLkeyword Unset
instance Minifiable MarginLeftP where
  minRep (MarginLeftP w) = MarginLeftP (minRep w)
  minRep x               = x

  minShow (MarginLeftP w) = minShow w
  minShow x               = pack $ show x

data MarginP = Mkeyword Csswide
             | Margin1 MarginWidth
             | Margin2 MarginWidth MarginWidth
             | Margin3 MarginWidth MarginWidth MarginWidth
             | Margin4 MarginWidth MarginWidth MarginWidth MarginWidth
instance Show MarginP where
  show (Mkeyword k) = show k
  show (Margin1 a)       = show a
  show (Margin2 a b)     = mconcat [show a, " ", show b]
  show (Margin3 a b c)   = mconcat [show a, " ", show b, " ", show c]
  show (Margin4 a b c d) = mconcat [show a, " ", show b, " ", show c, " ", show d]
instance Property MarginP where
  initial = Mkeyword Initial
  inherit = Mkeyword Inherit
  unset   = Mkeyword Unset
instance Minifiable MarginP where
  minRep (Margin1 a)       = Margin1 (minRep a)
  minRep (Margin2 a b)     = Margin2 (minRep a) (minRep b)
  minRep (Margin3 a b c)   = Margin3 (minRep a) (minRep b) (minRep c)
  minRep (Margin4 a b c d) = Margin4 (minRep a) (minRep b) (minRep c) (minRep d)
  minRep x = x

  minShow (Margin1 a)       = mconcat [minShow a]
  minShow (Margin2 a b)     = mconcat [minShow a, " ", minShow b]
  minShow (Margin3 a b c)   = mconcat [minShow a, " ", minShow b, " ", minShow c]
  minShow (Margin4 a b c d) = mconcat [minShow a, " ", minShow b, " ", minShow c, " ", minShow d]
  minShow x                 = pack $ show x

instance Shorthand Margin where
  expand (Margin a NoValue NoValue NoValue) = top ++ v ++ right ++ v ++ bottom ++ v ++ left ++ v
    where v = show a ++ ";"
  expand (Margin a b NoValue NoValue) = top ++ v1 ++ right ++ v2 ++ bottom ++ v1 ++ left ++ v2
    where v1 = show a ++ ";"
          v2 = show b ++ ";"
  expand (Margin a b c NoValue) = top    ++ show a ++ ";" ++ right ++ show b ++ ";"
                               ++ bottom ++ show c ++ ";" ++ left  ++ show b ++ ";"
  expand (Margin a b c d) = top    ++ show a ++ ";" ++ right ++ show b ++ ";"
                         ++ bottom ++ show c ++ ";" ++ left  ++ show d ++ ";"


-- test = Margin {first = Value (Length (Distance 10 PX)), second = Value Auto, third = NoValue, fourth = NoValue}
-}

