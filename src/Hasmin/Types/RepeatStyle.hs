{-# LANGUAGE OverloadedStrings #-}

module Hasmin.Types.RepeatStyle where
import Hasmin.Types.Class
import Data.Monoid ((<>))
import Data.Text.Lazy.Builder (singleton)
import Control.Monad.Reader (ask)

data RepeatStyle = RepeatX 
                 | RepeatY
                 | RSPair RSKeyword (Maybe RSKeyword)
  deriving (Show)

data RSKeyword = RS_Repeat | RS_Space | RS_Round | RS_NoRepeat
  deriving (Eq, Show)

instance Eq RepeatStyle where
  RepeatX == RepeatX = True
  a@RepeatX == b@RSPair{} = b == a
  RepeatY == RepeatY = True
  a@RepeatY == b@RSPair{} = b == a
  RSPair RS_NoRepeat (Just RS_Repeat) == RepeatY = True
  RSPair RS_Repeat (Just RS_NoRepeat) == RepeatX = True
  RSPair RS_NoRepeat (Just RS_Repeat) == RSPair RS_NoRepeat (Just RS_Repeat) = True
  RSPair RS_Repeat (Just RS_NoRepeat) == RSPair RS_Repeat (Just RS_NoRepeat) = True
  RSPair RS_Space (Just RS_Space) == RSPair RS_Space Nothing = True
  RSPair RS_Space (Just RS_Space) == RSPair RS_Space (Just RS_Space) = True
  RSPair RS_Round (Just RS_Round) == RSPair RS_Round Nothing = True
  RSPair RS_Round (Just RS_Round) == RSPair RS_Round (Just RS_Round) = True
  RSPair RS_NoRepeat (Just RS_NoRepeat) == RSPair RS_NoRepeat Nothing = True
  RSPair RS_NoRepeat (Just RS_NoRepeat) == RSPair RS_NoRepeat (Just RS_NoRepeat) = True
  RSPair RS_Repeat (Just RS_Repeat) == RSPair RS_Repeat Nothing = True
  RSPair RS_Repeat (Just RS_Repeat) == RSPair RS_Repeat (Just RS_Repeat) = True
  RSPair x Nothing == RSPair y Nothing = x == y
  a@(RSPair _ Nothing) == b@(RSPair _ _) = b == a
  _ == _ = False
  

instance ToText RSKeyword where
  toBuilder RS_Repeat   = "repeat"
  toBuilder RS_Space    = "space"
  toBuilder RS_Round    = "round"
  toBuilder RS_NoRepeat = "no-repeat"

instance ToText RepeatStyle where
  toBuilder RepeatX = "repeat-x"
  toBuilder RepeatY = "repeat-y"
  toBuilder (RSPair r1 r2 ) = toBuilder r1 <> maybe mempty (\x -> singleton ' ' <> toBuilder x) r2

instance Minifiable RepeatStyle where
  minifyWith r = do
    conf <- ask
    pure $ if True {- shouldMinifyRepeatStyle conf -}
              then minifyRepeatStyle r
              else r

minifyRepeatStyle :: RepeatStyle -> RepeatStyle
minifyRepeatStyle (RSPair RS_Repeat (Just RS_NoRepeat)) = RepeatX
minifyRepeatStyle (RSPair RS_NoRepeat (Just RS_Repeat)) = RepeatY
minifyRepeatStyle (RSPair x (Just y)) = if x == y
                                           then RSPair x Nothing
                                           else RSPair x (Just y)
minifyRepeatStyle x = x
