{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}

module Hasmin.Types.PercentageLength where

import Hasmin.Types.Dimension
import Hasmin.Types.Numeric
import Hasmin.Types.Class

-- | CSS <length-percentage> data type, i.e.: [length | percentage]
-- Though because of the name it would be more intuitive to define:
-- type LengthPercentage = Either Distance Percentage,
-- we invert them to make use of Either's functor instance, since it makes no
-- sense to minify a Percentage.
type PercentageLength = Either Percentage Distance

instance Minifiable PercentageLength where
  minifyWith x@(Right _) = mapM minifyWith x
  minifyWith x@(Left p) | p == 0    = pure $ Right (Distance 0 Q) -- minifies 0% to 0
                        | otherwise = pure x

isNonZeroPercentage :: PercentageLength -> Bool
isNonZeroPercentage (Left p) = p /= 0
isNonZeroPercentage _        = False

isZero :: (Num a, Eq a) => Either a Distance -> Bool
isZero = either (== 0) (== Distance 0 Q)


